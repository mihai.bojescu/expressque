import { Server } from 'http'
import { ExternalRouter } from 'private/router'
import { AndCallback } from 'private/server'
import { ServerDecorator } from 'public/decorators'
import { Application } from 'public/server'

export function BasicServerDecorator (
  externalRouter: ExternalRouter
): ServerDecorator {
  return {
    decorate
  }

  function decorate (server: Server): Application {
    return {
      ...externalRouter,
      listen: listenFunction(server)
    }
  }

  function listenFunction (server: Server): Application['listen'] {
    return (port) => {
      server.listen(port)

      return { and: andFunction() }
    }
  }

  function andFunction (): AndCallback {
    return (callback) => callback()
  }
}
