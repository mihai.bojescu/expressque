import { errors, ErrorType, ErrorValue } from 'private/errors'
import { ServerError } from 'public/errors'

export function ServerErrorBuilder (type: ErrorType) {
  const constructor = errors[type]

  return Builder(constructor)
}

function Builder<T extends (...args: any[]) => any>(
  constructor: T
): (...args: Parameters<T>) => ServerError {
  return (...args) => {
    const results = constructor(...args) as ErrorValue
    return new ServerError(results.reason, results.status, results.body)
  }
}
