import { ErrorValue } from './types'

const httpErrors = {
  400: { status: 400, body: { error: 'Bad parameters' } },
  401: { status: 401, body: { error: 'Unauthorized' } },
  403: { status: 403, body: { error: 'Forbidden' } },
  404: { status: 404, body: { error: 'Not found' } },
  500: { status: 500, body: { error: 'Internal server error' } },
  502: { status: 502, body: { error: 'Bad gateway' } }
}

const reasons = {
  '0x0000': (path: string) => `[0x0000] Path '${path}' is not a valid path.`,
  '0x0001': (method: string, path: string) => `[0x0001] Route '${method.toUpperCase()} ${path}' got overwritten.`,
  '0x0002': (method: string, path: string) => `[0x0002] Route '${method.toUpperCase()} ${path}' is not found.`,
  '0x0003': (method: string | undefined, path: string | undefined) => `[0x0003] Route '${method?.toUpperCase()} ${path}' is not found.`,
  '0x0004': (method: string | undefined, path: string | undefined) => `[0x0004] Server didn't send a response for '${method?.toUpperCase()} ${path}'.`,
  '0x0005': (method: string | undefined, path: string | undefined, err: Error) => `[0x0005] Could not parse path parameters for '${method?.toUpperCase()} ${path}': ${err.message}`,
  '0x0006': (method: string | undefined, path: string | undefined, err: Error) => `[0x0006] Could not parse query parameters for '${method?.toUpperCase()} ${path}': ${err.message}`,
  '0x0007': (method: string | undefined, path: string | undefined, err: Error) => `[0x0007] Could not parse body for '${method?.toUpperCase()} ${path}': ${err.message}`
}

export const errors = {
  '0x0000': (path: string): ErrorValue => ({ reason: reasons['0x0000'](path), ...httpErrors[401] }),
  '0x0001': (method: string, path: string): ErrorValue => ({ reason: reasons['0x0001'](method, path), ...httpErrors[500] }),
  '0x0002': (method: string, path: string): ErrorValue => ({ reason: reasons['0x0002'](method, path), ...httpErrors[404] }),
  '0x0003': (method: string | undefined, path: string | undefined): ErrorValue => ({ reason: reasons['0x0003'](method, path), ...httpErrors[404] }),
  '0x0004': (method: string | undefined, path: string | undefined): ErrorValue => ({ reason: reasons['0x0004'](method, path), ...httpErrors[500] }),
  '0x0005': (method: string | undefined, path: string | undefined, err: Error): ErrorValue => ({ reason: reasons['0x0005'](method, path, err), ...httpErrors[500] }),
  '0x0006': (method: string | undefined, path: string | undefined, err: Error): ErrorValue => ({ reason: reasons['0x0006'](method, path, err), ...httpErrors[500] }),
  '0x0007': (method: string | undefined, path: string | undefined, err: Error): ErrorValue => ({ reason: reasons['0x0007'](method, path, err), ...httpErrors[500] })
}
