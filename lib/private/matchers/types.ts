import { Path } from 'private/router'
import { Request, Response } from 'public/middleware'

export interface Matcher {
  match: <TRequest extends Request, TResponse extends Response>(
    paths: Path<TRequest, TResponse>,
    url: string
  ) => keyof Path<TRequest, TResponse> | undefined;
}
