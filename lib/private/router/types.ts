/* eslint-disable no-unused-vars */
import { Middleware, Request, Response } from 'public/middleware'

export enum Verb {
  GET = 'GET',
  POST = 'POST',
  PUT = 'PUT',
  PATCH = 'PATCH',
  DELETE = 'DELETE',
}

export type Path<TRequest extends Request, TResponse extends Response> = {
  [key: string]: Middleware<TRequest, TResponse>[];
};

export type Routes = {
  [Verb.GET]: Path<any, any>;
  [Verb.POST]: Path<any, any>;
  [Verb.PUT]: Path<any, any>;
  [Verb.PATCH]: Path<any, any>;
  [Verb.DELETE]: Path<any, any>;
};

export interface ExternalRouter {
  get: <TRequest extends Request, TResponse extends Response>(
    path: string,
    ...middlewares: Middleware<TRequest, TResponse>[]
  ) => void;
  post: <TRequest extends Request, TResponse extends Response>(
    path: string,
    ...middlewares: Middleware<TRequest, TResponse>[]
  ) => void;
  put: <TRequest extends Request, TResponse extends Response>(
    path: string,
    ...middlewares: Middleware<TRequest, TResponse>[]
  ) => void;
  patch: <TRequest extends Request, TResponse extends Response>(
    path: string,
    ...middlewares: Middleware<TRequest, TResponse>[]
  ) => void;
  delete: <TRequest extends Request, TResponse extends Response>(
    path: string,
    ...middlewares: Middleware<TRequest, TResponse>[]
  ) => void;
}

export interface InternalRouter extends ExternalRouter {
  getExternalRouter: () => ExternalRouter;
  route: <TRequest extends Request, TResponse extends Response>(
    method: string | undefined,
    path: string | undefined
  ) => [string, Middleware<TRequest, TResponse>[]];
}
