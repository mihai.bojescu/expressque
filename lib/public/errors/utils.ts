import { ServerError } from './serverError'

export function isServerError (err: Error | ServerError): err is ServerError {
  return err instanceof ServerError
}
