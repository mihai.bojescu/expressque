import { IncomingMessage, ServerResponse } from 'http'

export type Request<
  Headers = Record<string, string>,
  Params = Record<string, string>,
  Query = Record<string, unknown>,
  Body = Record<string, unknown>
> = IncomingMessage & {
  headers: Headers;
  params: Params;
  query: Query;
  body: Body;
};

export type Response = ServerResponse & {
  finishWith: () => { status: Response['status'] };
  withoutFinishing: () => { status: Response['status'] };
  status: (code: number) => { send: Response['send'] };
  send: (
    body?:
      | unknown
      | Record<string, unknown>
      | (unknown | Record<string, unknown>)[]
  ) => void;
  done: boolean;
};

export type Middleware<TRequest extends Request, TResponse extends Response> = (
  req: TRequest,
  res: TResponse
) => Promise<void>;
