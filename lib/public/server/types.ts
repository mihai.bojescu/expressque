import { ExternalRouter } from 'private/router'
import { AndCallback } from 'private/server'

export type Application = ExternalRouter & {
  listen: (
    port: number
  ) => {
    and: AndCallback;
  };
};
